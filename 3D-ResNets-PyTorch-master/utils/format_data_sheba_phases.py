import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import scipy.misc
import glob
import os
import re

# Organize the data according to the right format for video classification
# Convert png to jpg
# Create fold splits text files

# Images are generated from prepareFullSizeImages.py, genrate_full_and_roi_images()


labels = {'cyst': 1, 'metastasis': 2, 'hemangioma': 3, 'hcc': 4, 'fnh': 5, 'cholang': 6, 'healthy': 7}

full_dir = '/media/noa/2AB4478CB4475989/Noa/Liver4Phases/Datasets/fold_full_and_roi_images/Images'
save_dir = '/media/noa/2AB4478CB4475989/Noa/videoclassification/3D-ResNets-PyTorch-master/data/sheba_phases_videos/jpg'
split_dir = '/media/noa/2AB4478CB4475989/Noa/videoclassification/3D-ResNets-PyTorch-master/data/sheba_phases_videos/annotations'

file_format_jpg = 'image_id_%s_%s_phase_%s.jpg'
file_format_png = 'Image_id_%s_%s_phase_%s.png'
fold_dir = ['fold_#1', 'fold_#2', 'fold_#3']
N = (96, 96, 94)  # number of different "videos" per fold


for i in range(len(fold_dir)):
    for l in labels.keys():

        # Init Split files
        if i == 0:
            f = open(os.path.join(split_dir, l + "_test_split1.txt"), "w+")
        else:
            f = open(os.path.join(split_dir, l + "_test_split1.txt"), "a")

        il = labels[l]
        curr_dir = os.path.join(full_dir, fold_dir[i], str(il))
        for im_path in glob.glob(curr_dir + '/*_phase_0.png'):

            # Load all phase images
            im_arte = Image.open(im_path)
            filename = im_path.split('/')[-1].split('.')[0]
            num_arte = re.findall('\d+', filename)

            num_port = tuple([str(int(x) + t) for (x, t) in zip(num_arte, [0, N[i], 1])])
            im_port = Image.open(os.path.join(curr_dir, file_format_png % (num_port)))

            num_late = tuple([str(int(x) + t) for (x, t) in zip(num_arte, [0, 2*N[i], 2])])
            im_late = Image.open(os.path.join(curr_dir, file_format_png % (num_late)))

            num_none = tuple([str(int(x) + t) for (x, t) in zip(num_arte, [0, 3*N[i], 3])])
            im_none = Image.open(os.path.join(curr_dir, file_format_png % (num_none)))

            # # Display 4 phase images (for debug)
            # plt.figure()
            # plt.subplot(221)
            # plt.imshow(im_arte)
            # plt.title('arterial')
            # plt.subplot(222)
            # plt.imshow(im_port)
            # plt.title('portal')
            # plt.subplot(223)
            # plt.imshow(im_late)
            # plt.title('latde')
            # plt.subplot(224)
            # plt.imshow(im_none)
            # plt.title('none')

            # Save as jpeg
            save_path = os.path.join(save_dir, l, num_arte[0] + '_' + num_arte[1])
            os.makedirs(save_path, exist_ok=True)
            num_arte = tuple([str(x) for x in num_arte])
            im_arte.save(os.path.join(save_path, file_format_jpg % num_arte), quality=100) # still quality degradation comparing to png
            im_port.save(os.path.join(save_path, file_format_jpg % num_port), quality=100)
            im_late.save(os.path.join(save_path, file_format_jpg % num_late), quality=100)
            im_none.save(os.path.join(save_path, file_format_jpg % num_none), quality=100)

            # Update split file
            f.write(num_arte[0] + '_' + num_arte[1] + ' ' + num_arte[0] + '\n')
        f.close()




